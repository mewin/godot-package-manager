extends ConfirmationDialog

var packages := [] setget _set_packages
var user_installed_packages := [] setget _set_user_installed_packages

onready var package_list := $scroll_container/package_list

func _ready():
	__update_package_list()

func __update_package_list():
	if !package_list: # not initialized yet
		return
	
	for child in package_list.get_children():
		child.queue_free()
	
	for package in packages:
		var child := Label.new()
		child.text = "%s v.%s" % [package.name, GodotPackageManager.format_version(package.version)]
		package_list.add_child(child)

func _set_packages(value : Array):
	packages = value
	__update_package_list()

func _set_user_installed_packages(value : Array):
	user_installed_packages = value
	__update_package_list()
