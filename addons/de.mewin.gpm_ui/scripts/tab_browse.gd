extends Control

onready var package_list := $tab_browse/package_list
onready var btn_install := $tab_browse/buttons/btn_install
onready var dialog_confirm_install := $dialog_confirm_install

#############
# overrides #
#############
func _ready():
	GodotPackageManager.connect_static("remote_packages_updated", self, "_on_remote_packages_updated")
	GodotPackageManager.connect_static("installed_packages_changed", self, "_on_installed_packages_changed")
	package_list.packages = GodotPackageManager.get_remote_packages()
	__update()

#################
# private stuff #
#################
func __update():
	var selected : Array = package_list.get_selected_packages()
	btn_install.disabled = selected.empty()

############
# handlers #
############
func _on_remote_packages_updated():
	package_list.packages = GodotPackageManager.get_remote_packages()

func _on_installed_packages_changed():
	package_list.packages = GodotPackageManager.get_remote_packages()

func _on_btn_install_pressed():
	var selected : Array = package_list.get_selected_packages()
	if selected:
		var full_list := GodotPackageManager.build_dependency_list(selected)
		dialog_confirm_install.user_installed_packages = selected
		dialog_confirm_install.packages = full_list
		dialog_confirm_install.popup_centered_ratio()
		

func _on_package_list_selection_changed():
	__update()

func _on_dialog_confirm_install_confirmed():
	GodotPackageManager.cr_install_packages(dialog_confirm_install.packages, \
			dialog_confirm_install.user_installed_packages)
