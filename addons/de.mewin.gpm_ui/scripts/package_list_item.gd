extends Control

var package : GPMPackage = null setget _set_package
var package_versions := [] setget _set_package_versions
var selected := false setget _set_selected

onready var chk_checked := $root/chk_checked
onready var tex_package_icon := $root/tex_package_icon
onready var btn_package_name := $root/cnt_info/cnt_name/btn_package_name
onready var lbl_package_installed := $root/cnt_info/cnt_name/lbl_package_installed
onready var btn_package_version := $root/cnt_info/cnt_version_author/btn_package_version
onready var btn_package_author := $root/cnt_info/cnt_version_author/btn_package_author
onready var cnt_categories := $root/cnt_info/cnt_categories
onready var lbl_categories := $root/cnt_info/cnt_categories/lbl_categories
onready var btn_repository := $root/cnt_info/cnt_more/btn_repository
onready var btn_state := $root/cnt_info/cnt_more/btn_state
onready var btn_license := $root/cnt_info/cnt_more/btn_license

var __installation_status := {}

#############
# overrides #
#############
func _ready():
	chk_checked.pressed = selected

	btn_package_version.get_popup().connect("about_to_show", self, "_on_btn_package_version_popup_about_to_show")	
	btn_package_version.get_popup().connect("index_pressed", self, "_on_btn_package_version_popup_index_pressed")	
	__fill()

func _gui_input(event):
	if event is InputEventMouseButton && !event.pressed && event.button_index == BUTTON_LEFT:
		if Rect2(Vector2(), rect_size).has_point(event.position):
			_set_selected(!selected)
		accept_event()

#################
# private stuff #
#################
func __fill():
	if package:
		__installation_status = GodotPackageManager.get_installation_status(package)
	
	if !tex_package_icon:
		return
	
	for i in range(1, cnt_categories.get_child_count()):
		cnt_categories.get_child(i).queue_free()
	
	if package:
		btn_package_name.text = package.name
		lbl_package_installed.text = "(%s)" % __format_installation_status()
		btn_package_version.text = tr("Version %s") % GodotPackageManager.format_version(package.version)
		btn_package_author.text = package.author
		btn_state.text = package.state
		btn_license.text = package.license
		
		if package.categories:
			lbl_categories.text = tr("Categories:")
			for category in package.categories:
				var btn_category : Button = btn_package_author.duplicate(0)
				btn_category.text = category
				btn_category.mouse_default_cursor_shape = CURSOR_POINTING_HAND
				btn_category.connect("pressed", self, "_on_filter_button_pressed", ["category:%s" % category])
				cnt_categories.add_child(btn_category)
		else:
			lbl_categories.text = tr("Uncategorized")
	__fill_versions()

func __fill_versions():
	if !btn_package_version:
		call_deferred("__fill_version")
		return
	var popup : PopupMenu = btn_package_version.get_popup()
	popup.clear()
	
	for version in package_versions:
		var idx := popup.get_item_count()
		popup.add_item(tr("Version %s") % GodotPackageManager.format_version(version.version))
		popup.set_item_metadata(idx, version)
	btn_package_version.select(0)

func __format_installation_status() -> String:
	if !__installation_status.get("installed", false):
		return tr("Not installed")
	elif __installation_status.get("as_dependency", false):
		return tr("Installed as dependency")
	else:
		return tr("Installed")

func __request_filter(filter):
	print(filter)
	emit_signal("filter_requested", filter)

###########
# setters #
###########
func _set_package(value : GPMPackage):
	if value != package:
		package = value
		__fill()

func _set_package_versions(value : Array):
	if value != package_versions:
		package_versions = value
		__fill_versions()

func _set_selected(value : bool):
	if value != selected:
		selected = value
		chk_checked.pressed = selected
		emit_signal("selected_changed")

############
# handlers #
############
func _on_btn_package_name_pressed():
	pass # Replace with function body.

func _on_btn_package_author_pressed():
	if package:
		__request_filter("author:\"%s\"" % package.author)

func _on_btn_state_pressed():
	if package:
		__request_filter("state:\"%s\"" % package.state)

func _on_btn_license_pressed():
	if package:
		__request_filter("license:%s" % package.license)

func _on_filter_button_pressed(filter : String):
	__request_filter(filter)

func _on_btn_package_version_popup_about_to_show():
	pass

func _on_btn_package_version_popup_index_pressed(index : int):
	assert(index < package_versions.size())
	_set_package(package_versions[index])
	emit_signal("selected_version_changed")

###########
# signals #
###########
signal filter_requested(filter)
signal selected_changed()
signal selected_version_changed()
