extends Control

onready var cnt_packages := $scroll_container/cnt_packages

var packages := [] setget _set_packages
var __filtered_packages := {}
var __sorted_names := []
var __selected_packages := {}

#############
# overrides #
#############
func _ready():
	__fill_packages()

################
# public stuff #
################
func get_selected_packages() -> Array:
	return __selected_packages.values()

#################
# private stuff #
#################
func __fill_packages():
	__filter_packages() # TODO: only do this when filter changes
	
	for child in cnt_packages.get_children():
		child.queue_free()
	
	for package_name in __sorted_names:
		var packages_ : Array = __filtered_packages[package_name]
		var package : GPMPackage = packages_[0]
		var list_item := preload("res://addons/de.mewin.gpm_ui/scenes/components/package_list_item.tscn").instance()
		list_item.package = package
		list_item.package_versions = packages_
		list_item.connect("selected_changed", self, "_on_item_selected_changed", [list_item])
		list_item.connect("selected_version_changed", self, "_on_item_selected_version_changed", [list_item])
		cnt_packages.add_child(list_item)
	
	if __selected_packages:
		__selected_packages = {}
		emit_signal("selection_changed")

func __filter_packages():
	__filtered_packages = {}
	for package in packages:
		assert(package is GPMPackage)
		if !__filtered_packages.has(package.name):
			__filtered_packages[package.name] = []
		__filtered_packages[package.name].append(package)
	
	for package_name in __filtered_packages:
		__filtered_packages[package_name].sort_custom(self, "__compare_package_versions")
	
	__sort_packages()

func __sort_packages():
	__sorted_names = __filtered_packages.keys()
	__sorted_names.sort_custom(self, "__compare_package_names")

func __compare_package_versions(pack0 : GPMPackage, pack1 : GPMPackage):
	return pack0.version > pack1.version

func __compare_package_names(name0 : String, name1 : String):
	var pack0 : GPMPackage = __filtered_packages[name0][0]
	var pack1 : GPMPackage = __filtered_packages[name1][0]
	
	return pack0.name < pack1.name

###########
# setters #
###########
func _set_packages(value : Array):
	if value != packages:
		packages = value
		__fill_packages()

############
# handlers #
############
func _on_item_selected_changed(list_item : Control):
	if list_item.selected:
		__selected_packages[list_item.package.name] = list_item.package
	else:
		__selected_packages.erase(list_item.package.name)
	emit_signal("selection_changed")

func _on_item_selected_version_changed(list_item : Control):
	if list_item.selected:
		__selected_packages[list_item.package.name] = list_item.package
		emit_signal("selection_changed")

###########
# signals #
###########
signal selection_changed()
