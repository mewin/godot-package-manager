tool
extends Container

export var use_minimum_size := false
export var gap := 0.0

#############
# overrides #
#############
func _notification(what):
	if what == NOTIFICATION_SORT_CHILDREN:
		__sort_children()

#################
# private stuff #
#################
func __expand_row(row : Array, current_x : float):
	if !row:
		return
	
	var weight_sum := 0.0
	var missing_width := rect_size.x - current_x
	
	if missing_width <= 0.0:
		return
	
	for child in row:
		if child.size_flags_horizontal & Control.SIZE_EXPAND:
			weight_sum += child.size_flags_stretch_ratio
	if weight_sum <= 0.0:
		return
	
	var move_by := 0.0
	for child in row:
		child.rect_position.x += move_by
		if child.size_flags_horizontal & Control.SIZE_EXPAND:
			var rel_weight = child.size_flags_stretch_ratio / weight_sum
			var extra_width = missing_width * rel_weight
			child.rect_size.x += extra_width
			move_by += extra_width

func __sort_children():
	var pos := Vector2()
	var row_height := 0.0
	var vp_rect := get_viewport_rect()
	
	var row := []
	
	for child in get_children():
		if !child.visible || !child is Control:
			continue
		
		if use_minimum_size:
			child.rect_size = child.get_combined_minimum_size()
		
		if pos.x > 0.0 && pos.x + child.rect_size.x > rect_size.x:
			__expand_row(row, pos.x)
			pos.x = 0.0
			pos.y += row_height + gap
			row_height = 0.0
			row.clear()
		
		row.append(child)
		
		child.rect_position = pos
		
		row_height = max(row_height, child.rect_size.y)
		pos.x += child.rect_size.x + gap
	
	__expand_row(row, pos.x)
	rect_min_size.y = pos.y + row_height
