extends Control

const COLUMN_NAME = 0
const COLUMN_URL  = 1

onready var tree_repositories := $v_box_container/tree_repositories
onready var edt_repo_name := $v_box_container2/grid_container/edt_repo_name
onready var edt_repo_url := $v_box_container2/grid_container/edt_repo_url
onready var chk_repo_local := $v_box_container2/chk_repo_local
onready var chk_disabled := $v_box_container2/chk_disabled

var __itm_project : TreeItem
var __itm_system : TreeItem

func _ready():
	__fill_repositories()
	__select_first()
	__update_editor()

func select(repository : GPMRepository, root : TreeItem = null) -> bool:
	if !root:
		root = tree_repositories.get_root()
	assert(root)
	if !root:
		return false
	
	while root:
		if root.get_metadata(0) == repository:
			root.select(0)
			return true
		
		var children := root.get_children()
		if children && select(repository, children):
			return true
		
		root = root.get_next()
		
	return false

func __select_first():
	var child : TreeItem = __itm_system.get_children()
	if !child:
		child = __itm_project.get_children()
	if child:
		child.select(0)

func __fill_repositories():
	tree_repositories.clear()
	
	tree_repositories.create_item() # root
	
	__itm_project = tree_repositories.create_item()
	__itm_project.set_text(0, tr("Project"))
	__itm_project.set_selectable(0, false)
	
	__itm_system = tree_repositories.create_item()
	__itm_system.set_text(0, tr("System"))
	__itm_system.set_selectable(0, false)
	
	var repos := GodotPackageManager.get_repositories()
	for ns in repos:
		var parent := __itm_system
		if ns == GodotPackageManager.NS_PROJECT:
			parent = __itm_project
		__fill_tree_item(parent, repos[ns])

func __fill_tree_item(parent : TreeItem, repos : Array):
	for repo in repos:
		var item : TreeItem = tree_repositories.create_item(parent)
		item.set_metadata(0, repo)
		__fill_repo_item(item)

func __fill_repo_item(item : TreeItem):
	var repo := item.get_metadata(0) as GPMRepository
	assert(repo)
	item.set_text(COLUMN_NAME, repo.name)
	item.set_text(COLUMN_URL, repo.url)
	
	var color := Color.white
	if repo.disabled:
		color = Color.darkgray
	item.set_custom_color(0, color)
	item.set_custom_color(1, color)

func __update_editor():
	var repo := __get_selected_repository()
	
	edt_repo_name.editable = repo != null
	edt_repo_url.editable = repo != null
	chk_repo_local.disabled = repo == null
	
	if !repo:
		return
	
	edt_repo_name.text = repo.name
	edt_repo_url.text = repo.url
	
	chk_repo_local.set_block_signals(true)
	chk_repo_local.pressed = repo.namespace == GodotPackageManager.NS_PROJECT
	chk_repo_local.set_block_signals(false)
	
	chk_disabled.set_block_signals(true)
	chk_disabled.pressed = repo.disabled
	chk_disabled.set_block_signals(false)

func __get_selected_repository() -> GPMRepository:
	var selected : TreeItem = tree_repositories.get_selected()
	if !selected:
		return null

	return selected.get_metadata(0) as GPMRepository

func _on_btn_add_repository_pressed():
	var repo_name := tr("New Repository")
	var repo := GodotPackageManager.add_repository(tr("New Repository"), "", GodotPackageManager.NS_SYSTEM)
	__fill_repositories()
	select(repo)

func _on_btn_remove_repository_pressed():
	var repo := __get_selected_repository()
	if repo:
		GodotPackageManager.remove_repository(repo)
		__fill_repositories()
		__select_first()

func _on_tree_repositories_item_selected():
	__update_editor()

func _on_tree_repositories_nothing_selected():
	__update_editor()

func _on_edt_repo_name_text_changed(new_text : String):
	var repo := __get_selected_repository()
	var selected : TreeItem = tree_repositories.get_selected()
	if !repo || !selected:
		assert(false)
		return
	repo.name = new_text
	__fill_repo_item(selected)


func _on_edt_repo_url_text_changed(new_text):
	var repo := __get_selected_repository()
	var selected : TreeItem = tree_repositories.get_selected()
	if !repo || !selected:
		assert(false)
		return
	repo.url = new_text
	__fill_repo_item(selected)


func _on_chk_repo_local_toggled(button_pressed : bool):
	var repo := __get_selected_repository()
	if !repo:
		assert(false)
		return
	
	if button_pressed:
		repo.namespace = GodotPackageManager.NS_PROJECT
	else:
		repo.namespace = GodotPackageManager.NS_SYSTEM
	
	__fill_repositories()
	select(repo)

func _on_chk_disabled_toggled(button_pressed : bool):
	var repo := __get_selected_repository()
	var selected : TreeItem = tree_repositories.get_selected()
	if !repo || !selected:
		assert(false)
		return
	
	repo.disabled = button_pressed
	__fill_repo_item(selected)
