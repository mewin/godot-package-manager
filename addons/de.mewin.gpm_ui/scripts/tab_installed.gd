extends VBoxContainer

onready var package_list := $package_list

func _ready():
	GodotPackageManager.connect_static("installed_packages_changed", self, "_on_installed_packages_changed")
	
	package_list.packages = GodotPackageManager.get_installed_packages()

############
# handlers #
############
func _on_installed_packages_changed():
	package_list.packages = GodotPackageManager.get_installed_packages()
