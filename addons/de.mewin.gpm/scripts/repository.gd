extends Reference

class_name GPMRepository

var name := "" setget _set_name
var url := "" setget _set_url
var disabled := false setget _set_disabled
var namespace := "" setget _set_namespace
var __state

#############
# overrides #
#############
func _init(name_ : String, url_ : String, namespace_ : String, disabled_ : bool, state_):
	name = name_
	url = url_
	namespace = namespace_
	disabled = disabled_
	__state = state_

func get_file_name() -> String:
	# TODO: normalize url
	return "%s__%s" % [GPMUtil.safe_filename(name), url.sha1_text()]

###########
# setters #
###########
func _set_name(value : String):
	if value != name:
		name = value
		__state.changed = true

func _set_url(value : String):
	if value != url:
		url = value
		__state.changed = true

func _set_disabled(value : bool):
	if value != disabled:
		disabled = value
		__state.changed = true

func _set_namespace(value : String):
	if value == namespace:
		return
	assert(__state.namespaces.has(namespace))
	assert(__state.namespaces.has(value))
	__state.namespaces[namespace].repositories.erase(self)
	# if !value in __state.namespaces:
	# 	__state.namespaces[value] = []
	__state.namespaces[value].repositories.append(self)
	namespace = value
	__state.changed = true
