extends Object

class_name GPMConstants

const SETTING_AUTO_UPDATE = "auto_update"
const REGEX_URL = "^(?:(?<proto>[a-zA-Z]+):\\/\\/)?(?<host>[^\\/:]+)(?::(?<port>[0-9]{1,5}))?(?<path>\\/[^\\?]*)?(?:\\?(?<query>.*))?$"
const REGEX_HTTP_DATE_RFC822 = "^(?<weekday>[A-Za-z]+)\\W*(?<day>[0-9]{1,2})\\s+(?<month>[A-Za-z]+)\\s+(?<year>[0-9]{4})\\s+(?<hour>[0-9]{1,2}):(?<minute>[0-9]{1,2}):(?<second>[0-9]{1,2})\\s+(?<timezone>.*)$"
const REGEX_HTTP_DATE_RFC850 = "^(?<weekday>[A-Za-z]+)\\W*(?<day>[0-9]{1,2})\\-(?<month>[A-Za-z]+)\\-(?<year>[0-9]{2})\\s+(?<hour>[0-9]{1,2}):(?<minute>[0-9]{1,2}):(?<second>[0-9]{1,2})\\s+(?<timezone>.*)$"
# Sun Nov  6 08:49:37 1994 
const REGEX_HTTP_DATE_ANSIC  = "^(?<weekday>[A-Za-z]+)\\s+(?<month>[A-Za-z]+)\\s+(?<day>[0-9]{1,2})\\s+(?<hour>[0-9]{1,2}):(?<minute>[0-9]{1,2}):(?<second>[0-9]{1,2})\\s+(?<year>[0-9]{4})$"
const INDEX_FILE_NAME = "index.json"
const INDEX_FILE_NAME_GGZ = INDEX_FILE_NAME + ".ggz"
const INDEX_FOLDER_NAME = "index"
const PACKAGES_FOLDER_NAME = "packages"

const PROJECT_CONFIG_FOLDER = "res://"
const INSTALLED_INDEX_PATH = PROJECT_CONFIG_FOLDER + ".gpm/installed.json"

func _init():
	assert(false) # should not be instanced
