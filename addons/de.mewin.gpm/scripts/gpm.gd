extends Node

class_name GodotPackageManager

const NS_PROJECT = "project"
const NS_SYSTEM  = "system"

const PRIORITY_PROJECT = 100
const PRIORITY_SYSTEM  = 0

const __STATE_META_KEY = "__gpm_state__"

class __Namespace:
	var name : String
	var directory : String
	var repositories := []

class __State:
	var settings := GPMSettings.new()
	var namespaces := {}
	var installed_packages := {}
	var remote_packages := []
	var changed := false
	
	signal remote_packages_updated()
	signal installed_packages_changed()

class __RemotePackage:
	extends GPMPackage
	var repository : GPMRepository

################
# public stuff #
################
static func initialize():
	var state := __get_state()
	var dir := Directory.new()
	
	if !dir.dir_exists(GPMConstants.PROJECT_CONFIG_FOLDER) \
			&& dir.make_dir_recursive(GPMConstants.PROJECT_CONFIG_FOLDER) != OK:
		GPMUtil.write_log_warn("Could not create project config folder.")
	
	read_installed_packages()
	if state.settings.get_value(GPMConstants.SETTING_AUTO_UPDATE, true):
		yield(cr_update_repositories(), "completed")
	parse_indices()

static func get_installed_packages() -> Array:
	var state := __get_state()
	var installed := []
#	var LICENSES := [GPMPackage.LICENSE_CC0, GPMPackage.LICENSE_GPL2, GPMPackage.LICENSE_MIT]
#	for i in range(190):
#		var pack := GPMPackage.new("Package %s" % (i % 17))
#		pack.author = "Mewin" if i % 2 == 0 else "Godette"
#		pack.version = build_version(i % 3, i % 8, i % 100)
#		pack.categories = ["Tool", "Script", "Library"]
#		pack.license = LICENSES[randi() % LICENSES.size()]
#		installed.append(pack)
	for installation_status in state.installed_packages.values():
		installed.append(installation_status["package"])
	
	return installed

static func get_remote_packages() -> Array:
	var state := __get_state()
	return state.remote_packages

static func find_remote_packages(package_name : String) -> Array:
	var remote_packages := get_remote_packages()
	var result := []
	
	for package in remote_packages:
		if package.name == package_name:
			result.append(package)
	return result

static func get_installation_status(package) -> Dictionary:
	if package is GPMPackage:
		return get_installation_status(package.name)
	var state := __get_state()
	if package in state.installed_packages:
		return state.installed_packages[package]
	else:
		return {
			"installed": false
		}

static func build_version(major : int, minor : int, patch : int) -> int:
	# GDscript uses 64 bit integers
	# we use 2 byte major, 2 byte minor and 4 byte patch version
	# first bit of major version is sign, therefore only 15 bits are available (should still be enough)
	assert(major <= 0x7FFF)
	assert(minor <= 0xFFFF)
	assert(patch <= 0xFFFFFFFF)
	return (major << 48 | minor << 32 | patch)

static func parse_version(version : int) -> Dictionary:
	assert(version >= 0)
	return {
		"major": (version >> 48),
		"minor": (version >> 32) & 0xFFFF,
		"patch": version & 0xFFFFFFFF
	}

static func format_version(version : int) -> String:
	return "{major}.{minor}.{patch}".format(parse_version(version))

static func add_namespace(ns_name : String, directory: String, priority := 0) -> bool:
	var state := __get_state()
	if state.namespaces.has(ns_name):
		return false
	var namespace := __Namespace.new()
	namespace.name = ns_name
	namespace.directory = directory
	state.namespaces[ns_name] = namespace
	
	state.settings.load_settings(ns_name, directory, priority)
	__deserialize_repositories(namespace, state.settings.get_value("repositories", [], TYPE_ARRAY, ns_name))
	return true

static func get_namespace_folder(ns_name : String) -> String:
	var state := __get_state()
	if !state.namespaces.has(ns_name):
		return ""
	return state.namespaces[ns_name].directory

static func store_settings():
	var state := __get_state()
	if state.changed:
		for ns_name in state.namespaces:
			state.settings.set_value("repositories", __serialize_repositories(state.namespaces[ns_name].repositories), ns_name)
		state.changed = false
	__get_state().settings.store_settings()

static func add_repository(name : String, url : String, ns_name : String) -> GPMRepository:
	var state := __get_state()
	if !state.namespaces.has(ns_name):
		assert(false)
		return null
		# state.repositories[namespace] = []
	var new_repo := GPMRepository.new(name, url, ns_name, false, state)
	state.namespaces[ns_name].repositories.append(new_repo)
	state.changed = true
	return new_repo

static func remove_repository(repository : GPMRepository) -> bool:
	var state := __get_state()
	for ns_name in state.namespaces:
		var repos : Array = state.namespaces[ns_name].repositories
		if repository in repos:
			repos.erase(repository)
			state.changed = true
			return true
	return false

static func find_repository(name : String, namespace : String) -> GPMRepository:
	var state := __get_state()
	if !state.namespaces.has(namespace):
		return null
	for repo in state.namespaces[namespace].repositories:
		if repo.name == name:
			return repo
	return null

static func get_repositories() -> Dictionary:
	var state := __get_state()
	var repos := {}
	for ns_name in state.namespaces:
		repos[ns_name] = state.namespaces[ns_name].repositories.duplicate()
	return repos

static func cr_update_repositories():
	var state := __get_state()
	
	for ns_name in state.namespaces:
		var namespace : __Namespace = state.namespaces[ns_name]
		for repo in namespace.repositories:
			var updater := GPMRepositoryUpdater.new()
			yield(updater.cr_update_repository(repo, namespace.directory.plus_file("cache")), "completed")
	
	yield(GPMUtil.get_tree(), "idle_frame")

static func parse_indices():
	var state := __get_state()
	
	for ns_name in state.namespaces:
		var namespace : __Namespace = state.namespaces[ns_name]
		for repository in namespace.repositories:
			__parse_index(namespace, repository)
	
	state.emit_signal("remote_packages_updated")

static func read_installed_packages():
	var state := __get_state()
	var file := File.new()
	
	if !file.file_exists(GPMConstants.INSTALLED_INDEX_PATH):
		return
	
	var err = file.open(GPMConstants.INSTALLED_INDEX_PATH, File.READ)
	if err != OK:
		GPMUtil.write_log_error("Could not read local index: could not open file for reading.")
		return
	__deserialize_installed_pacakges(file.get_as_text())
	file.close()

static func build_dependency_list(target_packages : Array) -> Array:
	var dependency_list := target_packages.duplicate()
	var in_list := {}
	for package in dependency_list:
		in_list[package.name] = package
	if not __build_dependency_list(dependency_list, in_list):
		return []
	return dependency_list

static func cr_install_packages(packages : Array, user_selection : Array):
	var installer = load("res://addons/de.mewin.gpm/scripts/package_installer.gd").new()
	installer.packages = packages
	
	if !installer.check_packages():
		yield(GPMUtil.get_tree(), "idle_frame") # return a GDScriptFunctionState
		return
	
	if !yield(installer.cr_install_packages(), "completed"):
		return
	
	var state := __get_state()
	for package in packages:
		__set_installation_status(package, true, !user_selection.has(package))
	__store_installed_packages()
	state.emit_signal("installed_packages_changed")

static func connect_static(sig : String, obj : Object, method : String, args := [], flags := 0) -> int:
	var state := __get_state()
	return state.connect(sig, obj, method, args, flags)

#################
# private stuff #
#################
static func __get_state() -> __State:
	if !Engine.get_main_loop().has_meta(__STATE_META_KEY):
		Engine.get_main_loop().set_meta(__STATE_META_KEY, __State.new())
	return Engine.get_main_loop().get_meta(__STATE_META_KEY)

static func __serialize_repositories(repos : Array) -> Array:
	var serialized := []
	for repo in repos:
		serialized.append({
			"name": repo.name,
			"url": repo.url,
			"disabled": repo.disabled
		})
	return serialized

static func __deserialize_repositories(namespace : __Namespace, repositories : Array):
	var state := __get_state()
	for repo_data in repositories:
		var repo := GPMRepository.new(repo_data["name"], repo_data["url"], namespace.name, repo_data.get("disabled", false), state)
		namespace.repositories.append(repo)

static func __parse_index(namespace : __Namespace, repository : GPMRepository):
	if repository.disabled:
		return
	var safe_name := repository.get_file_name()
	var index_file_name = namespace.directory \
			.plus_file("cache") \
			.plus_file(safe_name)\
			.plus_file(GPMConstants.INDEX_FOLDER_NAME) \
			.plus_file(GPMConstants.INDEX_FILE_NAME)
	var file := File.new()
	var err = file.open(index_file_name, File.READ)
	if err != OK:
		GPMUtil.write_log_error("Could not open repository index.")
		return
	# TODO: unicode errors occur here
	var text := file.get_as_text()
	var parsed_json = parse_json(text)
	if !parsed_json is Dictionary \
			|| !parsed_json.get("packages") is Array:
		GPMUtil.write_log_error("Could not read repository index: invalid format.")
		return
	
	# begin parsing
	var state := __get_state()
	for package_json in parsed_json["packages"]:
		var package := __RemotePackage.new()
		if !package.deserialize(package_json):
			continue
		package.repository = repository
		state.remote_packages.append(package)

static func __notify_repository_changed(repo : GPMRepository):
	__get_state().changed = true

static func __build_dependency_list(dependency_list : Array, in_list : Dictionary, start_idx := 0) -> bool:
	var to_append := []
	for i  in range(start_idx, dependency_list.size()):
		var package : GPMPackage = dependency_list[i]
		for dependency in package.dependencies:
			# is about to be installed
			if in_list.has(dependency.package_name):
				var installed : GPMPackage = in_list[dependency.package_name]
				if installed.version < dependency.min_version || \
						(dependency.max_version > -1 && installed.version > dependency.max_version):
					GPMUtil.write_log_error("Dependency conflict in package %s." % installed.name)
					return false
			# is already installed
			else:
				var installation_status := get_installation_status(dependency.package_name)
				if installation_status["installed"]:
					var installed_version : int = installation_status["package"].version
					if installed_version >= dependency.min_version && \
							(dependency.max_version < 0 || installed_version <= dependency.max_version):
						# already installed, we are fine
						continue
					else:
						# already installed, but invalid version
						GPMUtil.write_log_error("Dependency conflict in package %s." % dependency.package_name)
						return false
				else:
					# not inv
					var candidates := find_remote_packages(dependency.package_name)
					var max_candidate : GPMPackage
					for candidate in candidates:
						if max_candidate && candidate.version < max_candidate.version:
							continue
						if candidate.version < dependency.min_version || \
								(dependency.max_version > -1 && candidate.version > dependency.max_version):
							continue
						max_candidate = candidate
					if not max_candidate:
						GPMUtil.write_log_error("Could not find installation candidate for package %s." % dependency.package_name)
						return false
					to_append.append(max_candidate)
					in_list[max_candidate.name] = max_candidate
	
	if to_append.empty():
		return true
	
	var oldlen := dependency_list.size()
	for new_package in to_append:
		dependency_list.append(new_package)
	return __build_dependency_list(dependency_list, in_list, oldlen)

static func __set_installation_status(package : GPMPackage, installed : bool, as_dependency : bool):
	var state := __get_state()
	if not installed:
		state.installed_packages.erase(package.name)
		return
	else:
		state.installed_packages[package.name] = {
			"installed": true,
			"package": package,
			"as_dependency": as_dependency
		}
		return

static func __store_installed_packages():
	var state := __get_state()
	var file := File.new()
	
	var err = file.open(GPMConstants.INSTALLED_INDEX_PATH, File.WRITE)
	if err != OK:
		GPMUtil.write_log_error("Could not write local index: could not open file for writing.")
		return
	file.store_string(__serialize_installed_pacakges())
	file.close()

static func __deserialize_installed_pacakges(raw_text : String):
	var state := __get_state()
	var res := JSON.parse(raw_text)
	
	state.installed_packages = {}
	
	if res.error != OK:
		GPMUtil.write_log_error("Could not deserialize installed packages: invalid json.")
		GPMUtil.write_log_error(res.error_string)
		GPMUtil.write_log_error("At line %d" % [res.error_line])
		return
	if !(res.result is Dictionary):
		GPMUtil.write_log_error("Could not deserialize installed packages: invalid content (dictionary expected)")
		return
	if res.result.has("packages") && res.result["packages"] is Array:
		for package_json in res.result["packages"]:
			if !package_json is Dictionary:
				continue
			var package := GPMPackage.new()
			if !package.deserialize(package_json):
				continue
			__set_installation_status(package, true, package_json.get("as_dependency", false))
	
	state.emit_signal("installed_packages_changed")

static func __serialize_installed_pacakges() -> String:
	var state := __get_state()
	var json := {}
	
	json["packages"] = []
	for installation_status in state.installed_packages.values():
		var package : GPMPackage = installation_status["package"]
		var package_json : Dictionary = package.serialize()
		package_json["as_dependency"] = installation_status["as_dependency"]
		json["packages"].append(package_json)
	
	return to_json(json)
