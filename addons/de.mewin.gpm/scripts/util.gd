extends Object

class_name GPMUtil

func _init():
	assert(false) # should not be instanced

static func get_tree() -> SceneTree:
	var tree := Engine.get_main_loop() as SceneTree
	assert(tree)
	return tree

static func parse_http_weekday(http_weekday : String) -> int:
	match http_weekday.to_lower().substr(0, 3):
		"mon":
			return OS.DAY_MONDAY
		"tue":
			return OS.DAY_TUESDAY
		"wed":
			return OS.DAY_WEDNESDAY
		"thu":
			return OS.DAY_THURSDAY
		"fri":
			return OS.DAY_FRIDAY
		"sat":
			return OS.DAY_SATURDAY
		"sun":
			return OS.DAY_SUNDAY
		
		var invalid:
			printerr("invalid value for http weekday: %s" % invalid)
			return OS.DAY_MONDAY

static func parse_http_month(http_month : String) -> int:
	match http_month.to_lower().substr(0, 3):
		"jan":
			return OS.MONTH_JANUARY
		"feb":
			return OS.MONTH_FEBRUARY
		"mar":
			return OS.MONTH_MARCH
		"apr":
			return OS.MONTH_APRIL
		"may":
			return OS.MONTH_MAY
		"jun":
			return OS.MONTH_JUNE
		"jul":
			return OS.MONTH_JULY
		"aug":
			return OS.MONTH_AUGUST
		"sep":
			return OS.MONTH_SEPTEMBER
		"oct":
			return OS.MONTH_OCTOBER
		"nov":
			return OS.MONTH_NOVEMBER
		"dec":
			return OS.MONTH_DECEMBER
		
		var invalid:
			printerr("invalid value for http month: %s" % invalid)
			return OS.MONTH_JANUARY
			

static func parse_http_date(http_date : String) -> Dictionary:
	# according to https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html
	# there are three possible formats:
	# 
	# Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
	# Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
	# Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format

	var regex_date_rfc822 := RegEx.new() # TODO: dont recompile every time
	regex_date_rfc822.compile(GPMConstants.REGEX_HTTP_DATE_RFC822)
	
	var date_parsed := regex_date_rfc822.search(http_date)
	if date_parsed:
		var result = {}
		result["weekday"] = parse_http_weekday(date_parsed.get_string("weekday"))
		result["day"] = int(date_parsed.get_string("day"))
		result["month"] = parse_http_month(date_parsed.get_string("month"))
		result["year"] = int(date_parsed.get_string("year"))
		result["hour"] = int(date_parsed.get_string("hour"))
		result["minute"] = int(date_parsed.get_string("minute"))
		result["second"] = int(date_parsed.get_string("second"))
		result["timezone"] = date_parsed.get_string("timezone")
		result["dst"] = false
		return result
	var regex_date_rfc850 := RegEx.new()
	regex_date_rfc850.compile(GPMConstants.REGEX_HTTP_DATE_RFC850)
	
	date_parsed = regex_date_rfc850.search(http_date)
	if date_parsed:
		var result = {}
		var year = int(date_parsed.get_string("year"))
		# RFC 850 uses a two-digit date
		# so we just guess anything < 80 happened during the 21st century
		if year < 80:
			year += 2000
		else:
			year += 1900
		result["weekday"] = parse_http_weekday(date_parsed.get_string("weekday"))
		result["day"] = int(date_parsed.get_string("day"))
		result["month"] = parse_http_month(date_parsed.get_string("month"))
		result["year"] = year
		result["hour"] = int(date_parsed.get_string("hour"))
		result["minute"] = int(date_parsed.get_string("minute"))
		result["second"] = int(date_parsed.get_string("second"))
		result["timezone"] = date_parsed.get_string("timezone")
		result["dst"] = false
		return result
	
	var regex_date_ansic := RegEx.new() # TODO: dont recompile every time
	regex_date_ansic.compile(GPMConstants.REGEX_HTTP_DATE_ANSIC)
	
	date_parsed = regex_date_ansic.search(http_date)
	if date_parsed:
		var result = {}
		result["weekday"] = parse_http_weekday(date_parsed.get_string("weekday"))
		result["day"] = int(date_parsed.get_string("day"))
		result["month"] = parse_http_month(date_parsed.get_string("month"))
		result["year"] = int(date_parsed.get_string("year"))
		result["hour"] = int(date_parsed.get_string("hour"))
		result["minute"] = int(date_parsed.get_string("minute"))
		result["second"] = int(date_parsed.get_string("second"))
		result["timezone"] = date_parsed.get_string("timezone")
		result["dst"] = false
		return result
	
	printerr("parsing http date failed: invalid format: %s" % http_date)
	return {}

# formats a date according to ISO 8601
static func format_date(date : Dictionary) -> String:
	return "%4d-%02d-%02d %02d:%02d:%02d" % [
		date.get("year", 0),
		date.get("month", 0),
		date.get("day", 0),
		date.get("hour", 0),
		date.get("minute", 0),
		date.get("second", 0)
	]

static func is_valid_date(date : Dictionary) -> bool:
	return date.has("year") \
			&& date.has("month") \
			&& date.has("day") \
			&& date.has("hour") \
			&& date.has("minute") \
			&& date.has("second")

static func compare_dates(date0 : Dictionary, date1 : Dictionary) -> int:
	assert(is_valid_date(date0))
	assert(is_valid_date(date1))
	
	if date0["year"] != date1["year"]:
		return date0["year"] - date1["year"]
	if date0["month"] != date1["month"]:
		return date0["month"] - date1["month"]
	if date0["day"] != date1["day"]:
		return date0["day"] - date1["day"]
	if date0["hour"] != date1["hour"]:
		return date0["hour"] - date1["hour"]
	if date0["minute"] != date1["minute"]:
		return date0["minute"] - date1["minute"]
	return date0["second"] - date1["second"]

static func date_comparator(date0 : Dictionary, date1 : Dictionary) -> bool:
	return compare_dates(date0, date1) < 0

static func oct_to_int(string : String) -> int:
	var as_int := int(string)
	var res := 0
	var fac := 1
	while as_int > 0:
		var digit = as_int % 10
		if digit >= 8:
			return -1
		res += fac * digit
		fac *= 8
		as_int /= 10
	return res

static func safe_filename(filename : String) -> String:
	var unsafe_regex := RegEx.new()
	var res = unsafe_regex.compile("[^A-Za-z0-9_\\-\\.]+")
	assert(res == OK)
	return unsafe_regex.sub(filename, "_", true)

static func http_headers_to_dict(headers : PoolStringArray) -> Dictionary:
	var dict := {}
	for header in headers:
		var parts : PoolStringArray = header.split(":", true, 1)
		if parts.size() < 2:
			continue
		var name := parts[0].strip_edges().to_lower()
		dict[name] = parts[1]
	return dict

static func write_log(text : String):
	print("[GPM|I] %s" % text)

static func write_log_warn(text : String):
	printerr("[GPM|W] %s" % text)

static func write_log_error(text : String):
	printerr("[GPM|E] %s" % text)
