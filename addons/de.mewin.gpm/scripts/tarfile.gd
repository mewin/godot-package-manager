extends Reference

class_name GPMTarFile

const BUFFER_SIZE = 4096

enum FileType {
	NORMAL,
	HARD_LINK,
	SYMBOLIC_LINK,
	DIRECTORY = 5
}

class FileEntry:
	var name := ""
	var offset := 0
	var length := 0
	var type : int = FileType.NORMAL

var __file : File
var entries := []

func _init(file : File):
	__file = file

func collect_files() -> bool:
	var null_entry := false
	while true:
		if __file.get_position() <= __file.get_len() - 512:
			var raw_header := __file.get_buffer(512)
			# two consecutive null-entries mark the end of the file
			if __is_null(raw_header):
				if null_entry:
					return true
				null_entry = true
				continue
			null_entry = false
			
			var entry := __collect_file(raw_header)
			if !entry:
				return false
			var skip_size : int = int(ceil(entry.length / 512.0)) * 512 # must be a multiple of 512
			__file.seek(__file.get_position() + skip_size)
			entries.append(entry)
		else:
			return true
	return true

func cr_extract_files(target_dir : String) -> bool:
	# first create folders
	var dir := Directory.new()
	
	yield(GPMUtil.get_tree(), "idle_frame") # make this a coroutine
	
	for entry in entries:
		if entry.type == FileType.DIRECTORY:
			var full_name := target_dir.plus_file(entry.name)
			if dir.make_dir_recursive(full_name) != OK:
				GPMUtil.write_log_error("could not create folder: %s" % full_name)
				return false
	
	# then extract files
	for entry in entries:
		if entry.type == FileType.DIRECTORY:
			continue
		elif entry.type != FileType.NORMAL:
			GPMUtil.write_log_error("unsupported file type in archive: %s (%s)" \
					% [char(ord("0") + entry.type), entry.name])
			continue
		var full_name := target_dir.plus_file(entry.name)
		GPMUtil.write_log("Extracting file %s" % full_name)
		var out_file := File.new()
		if out_file.open(full_name, File.WRITE) != OK:
			GPMUtil.write_log_error("could not open file for writing")
			return false
		__file.seek(entry.offset)
		var remaining_bytes : int = entry.length
		while remaining_bytes > 0:
			if __file.eof_reached():
				GPMUtil.write_log_error("unexpected end of file while reading from archive")
				return false
			var buffer := __file.get_buffer(max(BUFFER_SIZE, remaining_bytes))
			if buffer.size() < 1:
				GPMUtil.write_log_error("error reading from archive")
				return false
			remaining_bytes -= buffer.size()
			out_file.store_buffer(buffer)
		out_file.close()
	return true

func __is_null(raw_header : PoolByteArray) -> bool:
	for byte in raw_header:
		if byte != 0:
			return false
	return true

func __collect_file(raw_header : PoolByteArray) -> FileEntry:
	assert(raw_header.size() == 512)
	var entry := FileEntry.new()
	var ustar := raw_header.subarray(257, 261).get_string_from_ascii() == "ustar"
	entry.offset = __file.get_position()
	entry.name = raw_header.subarray(0, 99).get_string_from_ascii()
	
	if ustar:
		var prefix := raw_header.subarray(345, 499).get_string_from_ascii()
		entry.name = prefix + entry.name
		var type := raw_header[156]
		if type != 0:
			entry.type = type - ord("0")
	
	var size_str := raw_header.subarray(124, 135).get_string_from_ascii()
	entry.length = GPMUtil.oct_to_int(size_str)
	if entry.length < 0:
		GPMUtil.write_log_error("invalid tar header: invalid file size: %s" \
				% size_str)
		return null
	
	return entry
