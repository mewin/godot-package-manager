extends Reference

class_name GPMPackageInstaller

var packages := []

var __http_request := HTTPRequest.new()

func _init():
	var tree := GPMUtil.get_tree()
	__http_request.use_threads = true
	__http_request.timeout = 1.0
	tree.root.call_deferred("add_child", __http_request)

func check_packages() -> bool:
	for package in packages:
		if !(package is GodotPackageManager.__RemotePackage):
			assert(false) # indicates script error
			return false
	# todo: test for conflicts
	return true

func cr_install_packages() -> bool:
	yield(GPMUtil.get_tree(), "idle_frame")
	var downloaded_files := []
	for package in packages:
		var repo : GPMRepository = package.repository
		var ns_dir := GodotPackageManager.get_namespace_folder(repo.namespace)
		if !ns_dir:
			assert(false)
			return false
		var downloaded_file : String = yield(__cr_download_package(package, ns_dir.plus_file("cache")), "completed")
		if !downloaded_file:
			return false
		downloaded_files.append(downloaded_file)
	
	for downloaded_file in downloaded_files:
		if !yield(__cr_install_package(downloaded_file), "completed"):
			# TODO: rollback if anythong goes wrong
			return false
	
	return true

func __cr_download_package(package : GPMPackage, cache_dir : String) -> String:
	# TODO: ggz
	
	var dir := Directory.new()
	var repository : GPMRepository = package.repository
	var safe_name := repository.get_file_name()
	var packages_cache_dir := cache_dir.plus_file(safe_name).plus_file(GPMConstants.PACKAGES_FOLDER_NAME)
	
	if !dir.dir_exists(packages_cache_dir) && dir.make_dir_recursive(packages_cache_dir) != OK:
		GPMUtil.write_log_error("Repository update failed: could not create packages cache dir")
		yield(GPMUtil.get_tree(), "idle_frame")
		return ""
	
	var package_file_name := "%s-%s.tar" % [package.name, GodotPackageManager.format_version(package.version)]
	var cached_file := packages_cache_dir.plus_file(package_file_name)
	
	if dir.file_exists(cached_file):
		GPMUtil.write_log("Package \"%s\" already exists locally at \"%s\", not redownloading." %\
				[package.name, cached_file])
		yield(GPMUtil.get_tree(), "idle_frame")
		return cached_file
	
	var remote_file := repository.url.plus_file("packages").plus_file(package.name).plus_file(package_file_name)
	
	GPMUtil.write_log("Downloading package \"%s\" from \"%s\" to \"%s\"..." % \
			[package.name, remote_file, cached_file])
	
	__http_request.download_file = cached_file
	if __http_request.request(remote_file, PoolStringArray(), true, HTTPClient.METHOD_GET) != OK:
		GPMUtil.write_log_error("Package download failed: could not create http request.")
		yield(GPMUtil.get_tree(), "idle_frame") # this function is expected to return a GDScriptFunctionState
		return ""
	
	var res = yield(__http_request, "request_completed")
	var result : int = res[0]
	var response_code : int = res[1]
#	var headers : Dictionary = GPMUtil.http_headers_to_dict(res[2])
	
	if response_code != 200:
		GPMUtil.write_log_error("Package download failed: server returned status: %d." % response_code)
		dir.remove(cached_file) # no error checking, just do our best
		return ""
	
	if dir.file_exists(cached_file):
		return cached_file
	return ""

func __cr_install_package(package_file : String) -> bool:
	var file := File.new()
	if !file.file_exists(package_file):
		GPMUtil.write_log_error("Cannot install package from file \"%s\": file does not exist." \
				% package_file)
		yield(GPMUtil.get_tree(), "idle_frame")
		return false
	var err : int
	if package_file.ends_with(".ggz"):
		err = file.open_compressed(package_file, File.READ, File.COMPRESSION_GZIP)
	else:
		err = file.open(package_file, File.READ)
	if err != OK:
		GPMUtil.write_log_error("Cannot install package from file \"%s\": cannot open file for reading." \
				% package_file)
		yield(GPMUtil.get_tree(), "idle_frame")
		return false
	
	var tar_file := GPMTarFile.new(file)
	if !tar_file.collect_files():
		GPMUtil.write_log_error("Cannot install package from file \"%s\": could not read tar headers." \
				% package_file)
		yield(GPMUtil.get_tree(), "idle_frame")
		return false
	
	# TODO: check if there is anything in the archive that does not belong there
	if !yield(tar_file.cr_extract_files("res://_tmp_"), "completed"):
		GPMUtil.write_log_error("Cannot install package from file \"%s\": could not extract files." \
				% package_file)
		return false
	
	yield(GPMUtil.get_tree(), "idle_frame")
	return true
