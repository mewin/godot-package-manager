extends Reference

class_name GPMPackage

const CATEGORY_TOOL     = "Tool"
const CATEGORY_SCRIPT   = "Script"
const CATEGORY_LIBRARY  = "Library"

const STATE_STABLE      = "stable"
const STATE_BETA        = "beta"
const STATE_DEVELOPMENT = "development"

const LICENSE_PROPRIETARY = "proprietary"
const LICENSE_MIT         = "MIT"
const LICENSE_GPL2        = "GPLv2"
const LICENSE_GPL3        = "GPLv3"
const LICENSE_LGPL2       = "LGPLv2"
const LICENSE_LGPL3       = "LGPLv3"
const LICENSE_MPL         = "MPL"
const LICENSE_APACHE      = "Apache"
const LICENSE_CC0         = "CC0"
# TODO: add more

class Dependency:
	var package_name : String
	var min_version : int
	var max_version : int
	var required : bool
	
	func _init(package_name_ : String, min_version_ : int, max_version_ : int, required_ : bool):
		package_name = package_name_
		min_version = min_version_
		max_version = max_version_
		required = required_
	
	func serialize() -> Dictionary:
		return {
			"package_name": package_name,
			"min_version": min_version,
			"max_version": max_version,
			"required": 1 if required else 0
		}

var name : String
var author := ""
var author_email := ""
var version := 0
var release_date := 0
var comment := ""
var categories := []
var icon_url := ""
var state := STATE_DEVELOPMENT
var license := LICENSE_PROPRIETARY
var dependencies := []
var files := []
var class_names := []

#############
# overrides #
#############
func _init(name_ := ""):
	name = name_

func _to_string():
	return str(serialize())

################
# public stuff #
################
func serialize() -> Dictionary:
	var dependencies_serialized := []
	
	for dependency in dependencies:
		dependencies_serialized.append(dependency.serialize())
	
	return {
		"name": name,
		"author": author,
		"author_email": author_email,
		"version": version,
		"release_date": release_date,
		"comment": comment,
		"categories": categories,
		"icon_url": icon_url,
		"state": state,
		"license": license,
		"dependencies": dependencies_serialized,
		"files": files,
		"class_names": class_names
	}

func deserialize(serialized : Dictionary) -> bool:
	if !serialized.has("name") || !serialized.has("author") || !serialized.has("author_email") \
			|| !serialized.has("version") || !serialized.has("files") || !serialized.has("categories") \
			|| !serialized.has("class_names"):
		GPMUtil.write_log_warn("cannot deserialize package: missing data")
		return false
	
	var deserialized_dependencies := []
	for dependency in serialized.get("dependencies", []):
		if !dependency is Dictionary || !dependency.has("package_name") || !dependency.has("min_version"):
			printerr("cannot deserialize package dependency: missing or invalid data")
			return false
		deserialized_dependencies.append(Dependency.new(str(dependency["package_name"]), int(dependency["min_version"]),
				int(dependency.get("max_version", -1)), int(dependency.get("required", 0)) > 0))
	
	for f in serialized["files"]:
		if !f is String:
			printerr("cannot deserialize package: invalid data type in file list")
			return false
			
	for c in serialized["categories"]:
		if !c is String:
			printerr("cannot deserialize package: invalid data type in category list")
			return false
			
	for c in serialized["class_names"]:
		if !c is String:
			printerr("cannot deserialize package: invalid data type in class name list")
			return false
	
	name = str(serialized["name"])
	author = str(serialized["author"])
	author_email = str(serialized["author_email"])
	version = int(serialized["version"])
	release_date = int(serialized.get("release_date", "0"))
	comment = str(serialized.get("comment", ""))
	categories = serialized["categories"].duplicate()
	icon_url = str(serialized.get("icon_url", ""))
	state = str(serialized.get("state", state))
	license = str(serialized.get("license", license))
	dependencies = deserialized_dependencies
	files = serialized["files"].duplicate()
	class_names = serialized["class_names"].duplicate()
	return true
