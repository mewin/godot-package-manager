extends Reference

class_name GPMRepositoryUpdater

const __INDEX_FILE_NAME_LAST_MODIFIED = GPMConstants.INDEX_FILE_NAME + ".last_modified"

var __http_request := HTTPRequest.new()
var __regex_url := RegEx.new()

func _init():
	var tree := GPMUtil.get_tree()
	__regex_url.compile(GPMConstants.REGEX_URL)
	__http_request.use_threads = true
	__http_request.timeout = 1.0
	tree.root.call_deferred("add_child", __http_request)

func cr_update_repository(repository : GPMRepository, cache_dir : String, force_update := false) -> bool:
	var tree := GPMUtil.get_tree()
	yield(tree, "idle_frame") # this function is expected to return a GDScriptFunctionState
	
	if !repository.url || repository.disabled:
		return true
	
	var dir := Directory.new()
	var safe_name := repository.get_file_name()
	var index_cache_dir := cache_dir.plus_file(safe_name).plus_file(GPMConstants.INDEX_FOLDER_NAME)
	if !dir.dir_exists(index_cache_dir) && dir.make_dir_recursive(index_cache_dir) != OK:
		GPMUtil.write_log_error("repository update failed: could not create index cache dir")
		return false
	
	var url_parsed := __regex_url.search(repository.url)
	if !url_parsed:
		GPMUtil.write_log_error("repository update failed: invalid repository url: %s" % repository.url)
		return false
	
	var proto := "http"
	if url_parsed.names.has("proto"):
		proto = url_parsed.get_string("proto").to_lower()
	match proto:
		"http", "https":
			return yield(__cr_update_repository_http(repository, url_parsed, index_cache_dir, force_update), "completed")
		_:
			GPMUtil.write_log_error("repository update failed: invalid protocol: %s" % proto)
			return false
	
	var index_file_name := index_cache_dir.plus_file(GPMConstants.INDEX_FILE_NAME_GGZ)
	if !dir.file_exists(index_file_name):
		GPMUtil.write_log_warn("No index file.")
		return false
	
	return true

func __cr_update_repository_http(repository : GPMRepository, url_parsed : RegExMatch, index_cache_dir : String, force_update := false) -> bool:
	yield(GPMUtil.get_tree(), "idle_frame") # ensure this is a coroutine
	
	# first check if the repository index is already up to date
	if !force_update && yield(__cr_update_repository_http_check_is_up_to_date(repository, url_parsed, index_cache_dir), "completed"):
		return true
	
	var index_url := url_parsed.get_string().plus_file(GPMConstants.INDEX_FILE_NAME_GGZ)
	
	# first try to download a compressed version of the index
	# TODO: ggz compression
	if false && yield(__cr_download_index_http(url_parsed, index_cache_dir, \
			GPMConstants.INDEX_FILE_NAME_GGZ), "completed"):
		return true
	
	# try to downloaded an uncompressed index
	if yield(__cr_download_index_http(url_parsed, index_cache_dir, \
			GPMConstants.INDEX_FILE_NAME), "completed"):
		return true
	
	return false

func __cr_download_index_http(url_parsed : RegExMatch, index_cache_dir : String, index_file_name : String) -> bool:
	var index_url := url_parsed.get_string().plus_file(index_file_name)
	GPMUtil.write_log("Downloading repository index from %s." % index_url)
	__http_request.download_file = index_cache_dir.plus_file(index_file_name)
	
	if __http_request.request(index_url, PoolStringArray(), true, HTTPClient.METHOD_GET) != OK:
		GPMUtil.write_log_error("repository update failed: could not create http request")
		yield(GPMUtil.get_tree(), "idle_frame") # this function is expected to return a GDScriptFunctionState
		return false
	
	var res = yield(__http_request, "request_completed")
	var result : int = res[0]
	var response_code : int = res[1]
	var headers : Dictionary = GPMUtil.http_headers_to_dict(res[2])
	
	if result != OK:
		GPMUtil.write_log_error("repository update failed: http request failed")
		return false
	
	if response_code != 200:
		GPMUtil.write_log_error("repository update failed: server status code is %d" % response_code)
		return false
	
	# store last modified time
	if headers.has("last-modified"):
		var last_modified := GPMUtil.parse_http_date(headers["last-modified"].strip_edges())
		if last_modified:
			var fname := index_cache_dir.plus_file(__INDEX_FILE_NAME_LAST_MODIFIED)
			var file := File.new()
			var err = file.open(fname, File.WRITE)
			if err != OK:
				GPMUtil.write_log_warn("Could not store index last modified time. Could not open %s." % fname)
			else:
				file.store_string(to_json(last_modified))
				file.close()
	
	return true

func __cr_update_repository_http_check_is_up_to_date(repository : GPMRepository, url_parsed : RegExMatch, index_cache_dir : String, force_update := false) -> bool:
	yield(GPMUtil.get_tree(), "idle_frame") # this function is expected to return a GDScriptFunctionState
	
	var dir := Directory.new()
	var local_index_fname := index_cache_dir.plus_file(__INDEX_FILE_NAME_LAST_MODIFIED)
	if !dir.file_exists(local_index_fname):
		GPMUtil.write_log("No modification date for repository \"%s\" stored." % repository.name)
		return false
	
	# get local index date
	var file = File.new()
	var err = file.open(local_index_fname, File.READ)
	if err != OK:
		GPMUtil.write_log_warn("Could not get local index date. Could not open %s." % local_index_fname)
		return false
	var local_index_date = parse_json(file.get_as_text())
	if !(local_index_date is Dictionary) || !GPMUtil.is_valid_date(local_index_date):
		GPMUtil.write_log_warn("Could not get local index date. Invalid file format.")
		return false
	
	# get remote index date
	var remote_index_date = yield(__cr_update_repository_http_get_server_modified_time(repository, url_parsed), "completed")
	if not remote_index_date:
		GPMUtil.write_log_warn("Could not retrieve remote repository index date.")
		return false
	
	GPMUtil.write_log("Repository \"%s\" last changed at %s (server time)." % [repository.name, GPMUtil.format_date(remote_index_date)])
	GPMUtil.write_log("Local index is from %s (server time)." % GPMUtil.format_date(local_index_date))
	
	if GPMUtil.compare_dates(local_index_date, remote_index_date) >= 0:
		GPMUtil.write_log("Local repository is up to date.")
		return true
	else:
		GPMUtil.write_log("Local repository is not up to date.")
	return false

func __cr_update_repository_http_get_server_modified_time(repository : GPMRepository, url_parsed : RegExMatch) -> Dictionary:
	if __http_request.request(url_parsed.get_string().plus_file(GPMConstants.INDEX_FILE_NAME), PoolStringArray(), true, HTTPClient.METHOD_HEAD) != OK:
		GPMUtil.write_log_error("repository update failed: could not create http request")
		yield(GPMUtil.get_tree(), "idle_frame") # this function is expected to return a GDScriptFunctionState
		return {}
	
	var res = yield(__http_request, "request_completed")
	var result : int = res[0]
	var response_code : int = res[1]
	var headers : PoolStringArray = res[2]
	
	if result != OK:
		GPMUtil.write_log_error("repository update failed: request failed")
		return {}
	if response_code != 200:
		GPMUtil.write_log_error("repository update failed: response status != 200")
		return {}
	
	var has_to_update := true
	for header in headers:
		var parts : PoolStringArray = header.split(":", true, 1)
		if parts.size() < 2:
			continue
		var name := parts[0].strip_edges().to_lower()
		if name != "last-modified":
			continue
		var parsed_date = GPMUtil.parse_http_date(parts[1].strip_edges())
		if parsed_date:
			return parsed_date
	return {}
