extends Reference

class_name GPMSettings

const SETTINGS_FILE_NAME = "settings.json"

class __Namespace:
	var name := ""
	var dir := ""
	var priority := 0
	var values := {}
	var changed := false

var __namespaces := []

################
# public stuff #
################
func has_value(name) -> bool:
	for ns in __namespaces:
		if ns.values.has(name):
			return true
	return false

func get_value(name, def = null, type = TYPE_NIL, namespace := ""):
	if type == TYPE_NIL && def != null:
		type = typeof(def)
	
	var val = __get_value(name, def, namespace)
	if type is Object:
		if !val is type:
			return def
	else:
		match type:
			TYPE_NIL:
				return val
			TYPE_STRING:
				val = str(val)
			TYPE_INT:
				val = int(val)
			TYPE_REAL:
				val = float(val)
			TYPE_BOOL:
				val = bool(val)
			_:
				if typeof(val) != type:
					return def
	if val is Array || val is Dictionary:
		val = val.duplicate()
	return val

func set_value(name, value, namespace):
	var ns : __Namespace
	for ns_ in __namespaces:
		if ns_.name == namespace:
			ns = ns_
	if !ns:
		printerr("cannot set value: namespace not found: %s" % namespace if namespace else "<none>")
		return
	if ns.values.get(name) != value:
		__set_value(name, value, ns)
		# __settings_changed = true
		print("setting \"%s\" changed to \"%s\"" % [name, value])

func get_all_values(name : String) -> Array:
	var values := []
	for ns in __namespaces:
		if ns.values.has(name):
			values.append({
				"namespace": ns.name,
				"value": ns.values[name]
			})
	return values

func collect_values(name : String) -> Array:
	var values := []
	for ns in __namespaces:
		if ns.values.has(name):
			var vals = ns.values[name]
			if vals is Array:
				for val in vals:
					values.append(val)
			else:
				values.append(vals)
	return values

func load_settings(namespace : String, settings_dir : String, priority := 0):
	for ns in __namespaces:
		if ns.name == namespace:
			__merge_dicts(ns.values, __load_settings(settings_dir))
			return
	var ns = __Namespace.new()
	ns.name = namespace
	ns.dir = settings_dir
	ns.priority = priority
	ns.values = __load_settings(settings_dir)
	
	var idx := 0
	while idx < __namespaces.size():
		if __namespaces[idx].priority < priority:
			break
		idx += 1
	__namespaces.insert(idx, ns)

func store_settings():
	for ns in __namespaces:
		if ns.changed:
			__store_namespace(ns)
			ns.changed = false

#################
# private stuff #
#################
func __load_settings(settings_dir : String) -> Dictionary:
	var file := File.new()
	if file.open(settings_dir.plus_file(SETTINGS_FILE_NAME), File.READ) != OK:
		return {}
	var data = parse_json(file.get_as_text())
	if !data is Dictionary:
		printerr("invalid settings data in %s" % settings_dir)
		return {}
	return data

func __set_value(name : String, value, ns : __Namespace):
	ns.values[name] = value
	ns.changed = true

func __get_value(key : String, def, namespace := ""):
	for ns in __namespaces:
		if ns.values.has(key) && (!namespace || ns.name == namespace):
			return ns.values[key]
	return def

func __store_namespace(namespace : __Namespace) -> int:
	var dir := Directory.new()
	if !namespace.dir:
		printerr("cannot store namespace without directory")
		return ERR_INVALID_PARAMETER
	if !dir.dir_exists(namespace.dir):
		var err = dir.make_dir_recursive(namespace.dir)
		if err != OK:
			printerr("could not create namespace directory: %s" % namespace.dir)
			return err
	var file := File.new()
	var fname := namespace.dir.plus_file(SETTINGS_FILE_NAME)
	var err = file.open(fname, File.WRITE)
	if err != OK:
		printerr("could not open file for writing: %s" % fname)
		return err
	file.store_string(JSON.print(namespace.values, "  "))
	return OK

static func __merge_dicts(dict_to : Dictionary, dict_from : Dictionary):
	for key in dict_from:
		dict_to[key] = dict_from[key]
