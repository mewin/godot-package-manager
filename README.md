# Godot Package Manager

**Note: the plugin is not finished and therefore cannot be used yet.**

Some things described here are still being implemented.

Package and dependency management for the Godot Engine. Manage project dependencies, create your own repositories and make use of a full-fledged package manager right inside the godot engine.

## Getting Started

These instructions will get you the package manager and its UI running for a single Godot project. As dependencies are project specific you will have to repeat this for any project you want to use the package manager with.

### Prerequisites

The project is developed and tested with the current stable release of the Godot Engine (3.2). Earlier releases are not tested and might not work. Also note that pre-3.2 releases of Godot had a bug that prevented HTTP HEAD requests and therefore do not work.

### Installing

First download and install the addons:

 1. The latest releases can be found [here](https://gitlab.com/mewin/godot-package-manager/-/releases).
 1. Click `Source code` and download the zip file.
 1. Extract the `addons` folder from the zip file into your project root folder.
 1. Open your project inside the Godot editor.
 1. Open the plugin list at `Project` -> `Project Settings...` -> `Plugins` tab
 1. Set `Godot Package Manager` and `Godot Package Manager UI` to `Active`

After that a new button should appear right next to the `AssetLib` button on the top. Click it and you will be able to install packages from there. For more information read the [documentation](https://mewin.de/projects/godot-package-manager/).

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/mewin/godot-package-manager/tags).

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://git.babulo.eu/mewin)

See also the list of [contributors](https://gitlab.com/mewin/godot-package-manager/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Anyone who worked on the [Godot Engine](https://godotengine.org/), it is a great project.
